const studenJson = [
  {
      "id": 1, 
      "apellidos": "CANTOS CEVALLOS",
      "nombres": "PABLO ALBARITO",
      "semestre": "PRIMERO",
      "paralelo": "A",
      "direccion": "Los esteros",
      "telefono": 0928388381,
      "correo": "pablito@gmail.com"
  },
  {
      "id": 2, 
      "apellidos": "CARDENAS ZAMBRANO",
      "nombres": "ALAN ALAIR",
      "semestre": "PRIMERO",
      "paralelo": "B",
      "direccion": "Las 7 puñaladas",
      "telefono": 0928388451,
      "correo": "alan@gmail.com"
  },
  {
      "id": 3, 
      "apellidos": "CARDENAS ZAMBRANO",
      "nombres": "JHOAN JOHAO",
      "semestre": "PRIMERO",
      "paralelo": "B",
      "direccion": "SAN AGUSTIN",
      "telefono": 0928458381,
      "correo": "johao@gmail.com"
  },
  {
      "id": 4,
      "apellidos": "CEVALLOS CARDENAS",
      "nombres": "ROCIO",
      "semestre": "PRIMERO",
      "paralelo": "A",
      "direccion": "LOS BAJOS",
      "telefono": 0998388385,
      "correo": "rocio@gmail.com"
  },
  {
      "id": 5,
      "apellidos": "GARCIA ZAMORA",
      "nombres": "PAULINA EMILY",
      "semestre": "SEGUNDO",
      "paralelo": "A",
      "direccion": "Los esteros",
      "telefono": 0928388381,
      "correo": "paulinao@gmail.com"
  },
  {
      "id": 6,
      "apellidos": "ZAMORA CEVALLOS",
      "nombres": "JUAN PABLO",
      "semestre": "SEGUNDO",
      "paralelo": "B",
      "direccion": "MANTA BEACH",
      "telefono": 0928388381,
      "correo": "juan@gmail.com"
  },
  {
      "id": 7,
      "apellidos": "CASTILLO ZAMBRANO",
      "nombres": "PANCHO JOSEHP",
      "semestre": "SEGUNDO",
      "paralelo": "A",
      "direccion": "LA PROAÑO",
      "telefono": 0928388381,
      "correo": "panchito@gmail.com"
  },
  {
      "id": 8,
      "apellidos": "JARAMILLO CASILLAS ",
      "nombres": "AURELIO",
      "semestre": "SEGUNDO",
      "paralelo": "B",
      "direccion": "PLAYITA MIA",
      "telefono": 0928388381,
      "correo": "aurelio@gmail.com"
  },
  {
      "id": 9,
      "apellidos": "MOREIRA SOLEDISPA",
      "nombres": "DANIEL ERICK",
      "semestre": "SEGUNDO",
      "paralelo": "A",
      "direccion": "BARRIO SAN JUAN",
      "telefono": 0928388381,
      "correo": "daniel@gmail.com"
  },
  {
      "id": 10,
      "apellidos": "DELGADO ESPINOZA",
      "nombres": "JOEL SEBASTIAN",
      "semestre": "SEGUNDO",
      "paralelo": "B",
      "direccion": "ELOY ALFARO",
      "telefono": 0928388381,
      "correo": "sebastian@gmail.com"
  }
]

studenJson.forEach((estudiante)=>{
  const elemento = document.querySelector('.box-estudent');
  const plantilla = document.createElement('div');
  plantilla.innerHTML = `<button class="name-estudiante" data-id="${estudiante.id}">${estudiante.apellidos} ${estudiante.nombres } </button>`;
  elemento.appendChild(plantilla);
})


const estudiantes = document.querySelectorAll('.name-estudiante');

estudiantes.forEach((estudiante)=>{
  estudiante.addEventListener('click', (nombre)=>{
      let id = nombre.target.getAttribute('data-id');
      studenJson.forEach((estudiante)=>{
          if(id == estudiante.id){
              const viewDetalle  = document.querySelector('.group-infor');
              viewDetalle.innerHTML = `
                  <p>Apellidos: <span>${estudiante.apellidos}</span></p>
                  <p>Nombres: <span>${estudiante.nombres}</span></p>
                  <p>Semestre: <span>${estudiante.semestre}</span></p>
                  <p>Paralelo: <span>${estudiante.paralelo}</span></p>
                  <p>Dirección: <span>${estudiante.direccion}</span></p>
                  <p>Teléfono: <span>${estudiante.telefono}</span></p>
                  <p>Correo Electrónico: <span>${estudiante.correo}</span></p>
              `;
          }
      })
  })
})
